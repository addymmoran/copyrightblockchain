/* 
	Name: CopyrightBlockchain.go
	Doc: https://addymmoran.github.io/ 
	Author: Addy Moran, addymmoran@gmail.com
	Reference: https://jeiwan.net/posts/building-blockchain-in-go-part-1/ 
*/

package main

import (
	"fmt"
 	"image"
 	_ "image/jpeg"
 	_ "image/gif"
 	_ "image/png"
 	"os"
 	"strconv"
 	"time"
 	"crypto/sha256" 
 	"bytes" 
)

/* Block signifies change in picture */
type Block struct {
	timestamp int64
	updatedImage []byte /* pixels */
	author []byte
	hash []byte
	previousHash []byte
}

/* Blockchain signifies summary of changes made to an image */
type BlockChain struct {
	blocks []*Block
}

/* Blockchain per image */ 
type Artwork struct {
	imageData []byte /* set to original file */ 
	blockchain BlockChain /* keeps track of all changes and what the imageData was at each change */
}

func createArtwork(imageFilename string, author string) *Artwork { /* genesis block */
	defaultHash := []byte{} 
	block := createBlock(author, imageFilename, defaultHash)
	blockchain := BlockChain{[]*Block{block}}
	return &Artwork{block.updatedImage, blockchain}
}

/* Reference: https://www.devdungeon.com/content/working-images-go#reading_image_from_file*/
func readImage(imageFilename string) []byte {
	reader, reader_err := os.Open(imageFilename)
	if reader_err != nil {
		fmt.Println("Error when trying to read " + imageFilename)
	}
	defer reader.Close()
	imageData, _, err := image.Decode(reader)
	if err != nil {
		fmt.Println("Error decoding " + imageFilename)
		fmt.Println(err)
		os.Exit(-1) // shows in console as 255 because UNIX converts to unsigned 8 bit val
	}
	content := fmt.Sprint(imageData)
	return []byte(content)
}

// implement view image based on imageData

func (block *Block) createHash() {
	timestamp := [] byte (strconv.FormatInt(block.timestamp, 10))
	headers := bytes.Join([][]byte{block.previousHash, block.updatedImage, block.author, 
		timestamp}, [] byte{})
	hash := sha256.Sum256(headers)
	block.hash = hash[:]
	fmt.Println("Created Hash")
}

func createBlock (author string, imageFilename string, previousHash []byte) *Block {
	imageData := readImage(imageFilename)
	block := &Block{time.Now().Unix(), imageData,[]byte(author), []byte{}, previousHash}
	block.createHash()
	fmt.Println("Created Block")
	return block
}

func (chain *BlockChain) submitChange(author string, imageFilename string) {
	previousBlock := chain.blocks[len(chain.blocks) - 1]
	newBlock := createBlock(author, imageFilename, previousBlock.hash)
	fmt.Println("Created new block")
	chain.blocks = append(chain.blocks, newBlock)
}


func main() {
	backcountryPerspectiveTest1 := createArtwork("../test/BP1_Original.jpeg", "Backcountry Perspective")
	//fmt.Println("Author of genesis block: " + string(backcountryPerspectiveTest1.blockchain.blocks[0].author))
	backcountryPerspectiveTest1.blockchain.submitChange("Addy Moran", "../test/BP1_GreyScale.jpeg")
	fmt.Println("Summary of Changes: ")
	for i := 0; i < len(backcountryPerspectiveTest1.blockchain.blocks); i++ {
		fmt.Println("Author of block: " + string(backcountryPerspectiveTest1.blockchain.blocks[i].author))
		fmt.Println("Hash of block: " + string(backcountryPerspectiveTest1.blockchain.blocks[i].hash))
		fmt.Println("Previous Hash of block: " + string(backcountryPerspectiveTest1.blockchain.blocks[i].previousHash))
	}
	checkDifferentContents := bytes.Compare(backcountryPerspectiveTest1.blockchain.blocks[0].updatedImage, backcountryPerspectiveTest1.blockchain.blocks[1].updatedImage)
	fmt.Println(checkDifferentContents) // if -1 then false
}

