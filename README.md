# Copyright Blockchain

## Purpose
By default the second you take a photo it is yours. However, proving that you are the one who hit the button can be difficult. Some technologies that can be used:
* EXIF data and your camera's serial number: working on the assumption the photo still contains EXIF data and that you have the same camera. 

This program is to allow a tracking system to allow the appropriate ownership/copyright. For example, if you are the photographer but you don't do the photo editing, you would have the first signature and then the photo editor would have the next one. Allowing the user to trae back who has edited the photo.

### This program currently supports the following image types
* PNG
* JPEG
* GIF

## Technologies Used
* Hyperledger 4
* Go
* CouchDB
* Node.JS (for the User Interface)

## Possible downfalls to this approach
* Every photo would need to be ran through through this Hyperledger node/network which is unrealistic. However, if used as an internal server or if it got integrated into a site like Instagram, Pinterest, etc. you could track your own photos and be able to prove you owned them. 